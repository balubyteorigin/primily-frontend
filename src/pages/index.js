import React from "react"
import { withPrefix, Link } from "gatsby"
import Helmet from "react-helmet"
// import Layout from "../components/layout"
import iphone from "../assets/images/iphone-w2.png"
// import SEO from "../components/seo"

const IndexPage = () => (
  <div>
        <Helmet>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        {/* <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> */}
        {/* <script src={withPrefix('jquery.js')} type="text/javascript" /> */}
        <script src={withPrefix('jquery.easing.1.3.js')} type="text/javascript" />
        <script src={withPrefix('jquery.fancybox.pack.js')} type="text/javascript" />
        <script src={withPrefix('jquery.fitvids.js')} type="text/javascript" />
        <script src={withPrefix('jquery.superslides.js')} type="text/javascript" />
        <script src={withPrefix('jquery.fitvids.js')} type="text/javascript" />
        <script src={withPrefix('script.js')} type="text/javascript" />
        <script src={withPrefix('bootstrap.min.js')} type="text/javascript" />
        <script src={withPrefix('nivo-lightbox.min.js')} type="text/javascript" />
        <script src={withPrefix('owl.carousel.min.js')} type="text/javascript" />
        {/* <script src={withPrefix('wow.min.js')} type="text/javascript" /> */}
    </Helmet>
  {/* <div className='preloader'><div className='loaded'>&nbsp;</div></div> */}
  <header id="home" className="header navbar-fixed-top">
      <div className="navbar navbar-default main-menu">

          <div className="container">
              <div className="navbar-header">
                  <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                      <span className="icon-bar"></span>
                      <span className="icon-bar"></span>
                      <span className="icon-bar"></span>
                  </button>
                  <a  href="#" className="navbar-brand "><img src={require('../assets/images/logo.png')} alt="Logo" /></a>
           
              </div>
              <div className="collapse navbar-collapse">
                  <ul className="nav navbar-nav navbar-right">
                      <li><a href="#works" className="">Features</a></li>
                      <li><a href="#describe" className="">Describe</a></li>
                      <li><a href="#pricing" className="">Pricing</a></li>
                      <li><a href="#demo" className="">Demo</a></li>
                      <li><a href="#about" className="">About Us</a></li>
                  </ul>

              </div>
          </div>
      </div> 
  </header>

  <section id="home">
      <div id="bgimage" className="header-image">
          <div className="container">

              <div className="row">
                  <div className="col-sm-5 col-xs-12 heading-text">
                      <div className="iphone center-content wow fadeInLeft" data-wow-duration="1s">
                          <img src={iphone} alt="" />
                      </div>
                  </div>
                  <div className="col-sm-7 col-xs-12 heading-text">
                      <div className="single_home_content wow zoomIn" data-wow-duration="1s">
          <h1>Introducing Advanced Virtual Answering Service (AVAS} for your Incoming Calls. </h1>
          <p className="bannerDescription">AVPS is very simple and low cost way to add automation for you <b>Business</b> or <b>Private</b> phone number.
                              Respondents call into a your phone number and through their keypad can get information and call routing automatically. No special hardware or system purchases are necessary</p>
                          <div className="button">
                          <a href="" className="btn">Get the iPhone APP</a>
            <a href="" className="btn white-btn youtube-media"><i className="fa fa-play"></i> Watch video</a>
          </div>
        </div>
                  </div>
              </div> 

          </div> 


      </div>
  </section>

  <section id="works" className="center-content">
      <div className="container">
          <div className="row">
              <div className="works_content text-center">

                  <div className="col-md-3 col-sm-6 col-xs-12">
                      <div className="single_works_text wow fadeInLeft" data-wow-duration=".5s">
                          <i className="fa fa-phone"></i>
                          <h3>Multiple Numbers</h3>
                          <p>One or more phone numbers. Selectable area codes and numbers.</p>
                      </div>
                  </div>

                  <div className="col-md-3 col-sm-6 col-xs-12">
                      <div className="single_works_text wow fadeInLeft" data-wow-duration=".8s">
                          <i className="fa fa-exclamation"></i>
                          <h3>Real Time Notifications</h3>
                          <p>Acknowledgement and notification using SMS and/or Email.</p>
                      </div>
                  </div>
                  <div className="col-md-3 col-sm-6 col-xs-12">
                      <div className="single_works_text wow fadeInLeft" data-wow-duration="1.2s">
                          <i className="fa fa-file-text-o"></i>
                          <h3>Call logging</h3>
                          <p>Help adjust you routing and messaging with Real-time call logging reports.</p>
                      </div>
                  </div>
                  <div className="col-md-3 col-sm-6 col-xs-12">
                      <div className="single_works_text wow fadeInLeft" data-wow-duration="1.5s">
                          <i className="fa fa-database"></i>
                          <h3>Easy Configuration </h3>
                          <p>Quick and Easy to modify Operating Hours, Holidays, Location, Services, Products and Call Routing.</p>
                      </div>
                  </div>
                  <div className="col-md-3 col-sm-6 col-xs-12">
                      <div className="single_works_text wow fadeInLeft" data-wow-duration="1.9s">
                          <i className="fa fa-text-width"></i>
                          <h3>Text to Speech</h3>
                          <p>Simple to create phone selection responses using Text-to-Speech technology.</p>
                      </div>
                  </div>
                  <div className="col-md-3 col-sm-6 col-xs-12">
                      <div className="single_works_text wow fadeInLeft" data-wow-duration="2.3s">
                          <i className="fa fa-money"></i>
                          <h3>Pay-As-You-Go</h3>
                          <p>No fixed monthly commitment. Low cost credits with SMS notification on remaining credits.</p>
                      </div>
                  </div>

                  <div className="col-md-3 col-sm-6 col-xs-12">
                      <div className="single_works_text wow fadeInLeft" data-wow-duration="2.6s">
                          <i className="fa fa-pie-chart"></i>
                          <h3>Real Tine Reporting</h3>
                          <p>View you call logs anytime from anywhere.</p>
                      </div>
                  </div>
                  <div className="col-md-3 col-sm-6 col-xs-12">
                      <div className="single_works_text wow fadeInLeft" data-wow-duration="2.9s">
                          <i className="fa fa-mobile"></i>
                          <h3>Mobile App for Routing</h3>
                          <p>Create and modify interactive selection routing amd responce using your modile device.</p>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>

  <section id="describe">
      <div className="container">
          <div className="row">
              <div className="works_2_content">
                  <div className="col-sm-4 col-xs-12">
                      <div className="works_2_iphone center-content">
                          <h3>No Lost Calls</h3><p>A major chunk of the business comes through phone and if any call goes unanswered, it simply means you are missing out on leads.</p>
                          <img src={require('../assets/images/routing.png')} alt="" />
                          <h3>How It Works</h3> <p>IVR relies on pre-recorded questions and responses for interacting with callers and direct calls to appropriate human agents. The IVR system prompts caller to select an option from a set menu using keypad responses and incoming telephone calls are routed to the relevant department.</p>
                      </div>
                  </div>
                  <div className="col-sm-8 col-xs-12 top-margin">
                      <div className="row">
                          <div className="single_works_2_content wow fadeInRight" data-wow-duration="1.5s">

                              <div className="col-sm-6 col-xs-12">
                                  <div className="single_works_2_text">
                                      <i className="fa fa-rocket"></i>
                                      <div className="text_deatels">
                                          <h3>What is Advanced Virtual Answering Service (AVAS)?</h3>
                                          <p>AVAS is a cloud service based on Interactive Voice Response (IVS) technology that enables interaction between a caller and our AVAS cloud via the telephone.
                                              <br />Callers can interact with our AVAS Cloud Solution by pressing numbers on their telephone keypad to get voice prompts to receive various information for for your customers.
                                              Common uses for our AVAS service include finding store locations, store hours, promotional specials, call routing to a department or person and other information
                                              to present to your customers.</p>
                                      </div>
                                  </div>
                              </div>
                              <div className="col-sm-6 col-xs-12">
              <div className="single_works_2_text">
                <i className="fa fa-cloud"></i>
                <div className="text_deatels">
                  <h3>Our AVAS Service</h3>
                  <p>We offer cloud hosted virtual mobile number solution that help customers to connect to the right department in any organization/business in short span of time and ensures no business call is lost</p>
                </div>
              </div>
            </div>

                              <div className="col-sm-6 col-xs-12">
                                  <div className="single_works_2_text">
                                      <i className="fa fa-phone"></i>
                                      <div className="text_deatels">
                                          <h3>Virtural Phone Number</h3>
                                          <p>Using a <b>Hosted Virtual Phone Number</b> in the cloud there is no hardware purchase. In minutes add additional numbers as you need them and in any area code you like.</p>
                                      </div>
                                  </div>
                              </div>

                              <div className="col-sm-6 col-xs-12">
                                  <div className="single_works_2_text">
                                      <i className="fa fa-money"></i>
                                      <div className="text_deatels">
                                          <h3>Pay as you Go and Grow</h3>
                                          <p>Start with a low cost Pay-as-you-go service and add additional services and feature as you business grows.</p>
                                      </div>
                                  </div>
                              </div>

            <div className="col-sm-6 col-xs-12">
              <div className="single_works_2_text">
                <i className="fa fa-code-fork"></i>
                <div className="text_deatels">
                  <h3>Simple Mobile Phone Designer</h3>
                  <p>The AVAS Designer allows you easily create and modify your calls routing according to your business needs from any mobile device.</p>
                </div>
              </div>
            </div>

                              <div className="col-sm-6 col-xs-12">
                                  <div className="single_works_2_text">
                                      <i className="fa fa-magic"></i>
                                      <div className="text_deatels">
                                          <h3>Automated Surveys</h3>
                                          <p>Create a automated voice survey to get feedback on on your service, products, etc.</p>
                                      </div>
                                  </div>
                              </div>

          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>

  <section id="video_icon">
      <div className="video_overlay">
          <div className="container">
              <div className="row">
                  <div className="col-sm-12 col-xs-12">
                      <div className="video_text center-content">
                       
                          <h5>ToDo -> Video, Automation, Carousel... With voice layover</h5>
                          <iframe width="1120" height="630" src="https://www.youtube.com/embed/mzW3382PST8" frameBorder="0" gesture="media" allow="encrypted-media" allowFullScreen></iframe>



                      </div>
                  </div>
              </div>
          </div>
      </div>	
  </section>


  <section id="description">
      <div className="container">
          <div className="row main_description">
              <div className="col-sm-6 col-xs-12">
                  <div className="left_desc_img center-content wow fadeInLeft" data-wow-duration="1.5s">
                      <img src={require('../assets/images/iphone-w1.png')} alt="" /><br></br>
                      <img src={require('../assets/images/d1.png')} height="67"
                           width="232"/>
                  </div>
              </div>
              <div className="col-sm-6">
                  <div className="right_desc_text top-margin wow fadeIn" data-wow-duration="1.5s">
                      <h1>[Description of the Mobile App]</h1>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer tincidunt efficitur vestibulum. Phasellus nisl leo, congue eu malesuada lobortis, fringilla et nulla.</p>

                      <div className="right_desc_bottom_text">
                          <div className="right_single_bottom_text">
                              <i className="fa fa-shield"></i>
                              <div className="right_bottom_description">
                                  <h6>Templates for Routing</h6>
                                  <p>Templates to get you started for various business. Easly modified for your business.</p>
                              </div>
                          </div>

                          <div className="right_single_bottom_text">
                              <i className="fa fa-css3"></i>
                              <div className="right_bottom_description">
                                  <h6>Phone Routing Designer</h6>
                                  <p>Easy to use mobile phone routing designer. Select and modify routing and messaging from anywhere.</p>
                              </div>
                          </div>

                          <div className="right_single_bottom_text">
                              <i className="fa fa-file-text"></i>
                              <div className="right_bottom_description">
                                  <h6>Any-Time-Anywhere Reporting</h6>
                                  <p>Easy to Understand Reports that can be access from anywhere at any time. Understand who is calling, what they accessed and how long it took.</p>
                              </div>
                          </div>

                          <div className="right_single_bottom_text">
                              <i className="fa fa-server"></i>
                              <div className="right_bottom_description">
                                  <h6>Pay-As-You-Go</h6>
                                  <p>No fixed monthly commitment. Low cost credits with SMS notification on remaining credits.</p>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>

  <section id="apps_gallery">
      [Apps Gallary]
      <div className="container">
          <div className="row">
              <div className="gallery_heading_text center-content">
                  <h1>Our Mobile Application Gallery</h1>
                  <p className="description"> [TODO-> Create mobile application screen shots]</p>
              </div>

              <div className="col-md-12 col-sm-12 col-xs-12"> 
                  <div className="gallery_carousel center-content">
                      <div className="single_gallery">
                          <img src={require('../assets/images/iphone-w1.png')} alt="" />
                      </div>
                      <div className="single_gallery">
                          <img src={require('../assets/images/iphone-w1.png')} alt="" />
                      </div>
                      <div className="single_gallery">
                          <img src={require('../assets/images/iphone-w1.png')} alt="" />
                      </div>
                      <div className="single_gallery">
                          <img src={require('../assets/images/iphone-w1.png')} alt="" />
                      </div>
                      <div className="single_gallery">
                          <img src={require('../assets/images/iphone-w1.png')} alt="" />
                      </div>
                      <div className="single_gallery">
                          <img src={require('../assets/images/iphone-w1.png')} alt="" />
                      </div>
                  </div>
              </div>
          </div>


      </div>
  </section>


<section id="pricing">
      <div className="container">
          <div className="row">
              <div className="pricing_heading_text center-content">
                  <h1>Our pricing is simple</h1>
                  <p>Except for our monthly $2/phone number, there are No Contracts or other monthly fees. <br />You purchase Credits and are notified as these run low.</p>
              </div>

              <div className="main_pricing_table">
                  <div className="col-md-6 col-sm-6 col-xs-12">
                      <div className="single_pricing wow fadeIn" data-wow-duration="1.5s">
                          <div className="top_text">
                              <h4>Free Real-Time Trial</h4>
                              <h6>No cost account creation</h6>
                              <p>Create an no cost account and try various routing configurations modified for your business.</p>
                          </div>
                          <ul>
                              <li><img src={require('../assets/images/tic.png')} alt="" />Store Hours</li>
                              <li><img src={require('../assets/images/tic.png')} alt="" />Location</li>
                              <li><img src={require('../assets/images/tic.png')} alt="" />Greeting</li>
                              <li><img src={require('../assets/images/tic.png')} alt="" />Free trial phone number</li>
                          </ul>
                          <hr />
                          <div className="table_info">
                              <div className="info_head_text">
                                  <sub>$</sub>0<span></span>
                              </div>
                              <a href="" className="btn-sm">sign up</a> <a href="" className="info_right_text">Free Live Trial</a>
                          </div>
                      </div>
                  </div>
                  </div>

                  <div className="col-md-6 col-sm-6 col-xs-12">
                      <div className="single_pricing economy_table wow fadeIn" data-wow-duration="1.6s">
                          <div className="top_text">
                              <h4>Base Credits</h4>
                              <h6> Credits</h6>
                              <p>No monthly commitment or payments. Purchase credits as you need them.</p>
                          </div>
                          <ul>
                              <li><img src={require('../assets/images/tic.png')} alt="" />Phone Number(s)</li>
                              <li><img src={require('../assets/images/tic.png')} alt="" />Fully customizable starter templates</li>
                              <li><img src={require('../assets/images/tic.png')} alt="" />Summary and Detailed Call Log Reports</li>
                              <li><img src={require('../assets/images/tic.png')} alt="" />Select you US area code</li>
                          </ul>
                          <hr />
                          <div className="table_info">
                              <div className="info_head_text">
                                  <sub>$</sub>20<span></span>
                              </div>
                              <a href="" className="btn-sm">sign up</a> <a href="" className="info_right_text">No monthly commitment.</a>
                          </div>
                      </div>
                  </div>
                </div>
              </div>
            </section>

  <section id="demo">
      <div className="container">
          <div className="row">
              <div className="download_heading_text center-content">
                  <h2>Create a free account to get a live working demo using you phone.</h2>
                  <p>When you create an account you are able to do some configuration on the greating and messaging, then do a live phone call to hear the experience you customer will have.</p>

                  <div className="down_text_des wow fadeInUp" data-wow-duration="1.5s">
                      <a href=""><img src={require('../assets/images/d1.png')} alt="" /></a>
                      <a href=""><img src={require('../assets/images/d2.png')} alt="" /></a>

                  </div>
              </div>
          </div>
      </div>
  </section>


  <section id="subscribe">
      <div className="subcribe_overlay">
          <div className="container">
              <div className="row">
                  <div className="subscribe_heading_text center-content">
                      <div className="subscribe_heading_title wow fadeIn" data-wow-duration="1s">
          <h1>Subscribe Now</h1>
          <p>Get latest news and offers curabitur posuere.</p>
        </div>

                      <div className="subcribe_form center-content">
                          <input type="text" placeholder="Email Address" />
                          <input type="submit" text="Subscribe" />
                      </div>
                  </div>


              </div>
          </div>
      </div>
  </section>


  <section id="message">
      <div className="container">
          <div className="row">
              <div className="col-md-12 col-sm-12">
                  <div className="message_content">
                      <div className="message_heading_text center-content wow zoomIn" data-wow-duration="1s">
                          <h2>Get in Touch</h2>
                          <p>Have feedback, suggestion, or any thought about our app? Feel free to contact us anytime, we will get back to you in 24 hours.</p>
                      </div>

                      <form action="#" id="formid" className="wow fadeIn" data-wow-duration="2s">
                          <div className="col-sm-6">
                              <div className="form-group">
                                  <input type="text" className="form-control" name="name" placeholder="first name" required="" />
                              </div>
                          </div>

                          <div className="col-sm-6">
                              <div className="form-group">
                                  <input type="email" className="form-control" name="email" placeholder="Email" required="" />
                              </div>
                          </div>

                          <div className="col-sm-12">
                              <div className="form-group">
                                  <input type="text" className="form-control" placeholder="Subject" />
                              </div>

                              <div className="form-group">
                                  <textarea className="form-control" name="message" rows="8" placeholder="Message"></textarea>
                              </div>

                              <div className="center-content">
                                  <input type="submit" value="Submit" className="btn larg-btn" />
                              </div>
                          </div>
                      </form>			
                  </div>
              </div>
          </div>


      </div>
  </section>

  <section id="about">
      <div className="container">
          <div className="row">
              <div className="col-md-12 col-sm-12">
                  <div className="message_content">
                      <div className="message_heading_text center-content wow zoomIn" data-wow-duration="1s">
                          <h2>About Us</h2>
                          <p>We are....</p>
                      </div>

                  </div>
              </div>
          </div>

      </div>
  </section>


  <footer id="footer" className="center-content">
      <div className="container">
          <div className="row">
              <div className="col-md-12 col-sm-12">
                  <div className="socail_bookmark">
                      <a href=""><i className="fa fa-twitter"></i></a>
                      <a href=""><i className="fa fa-facebook"></i></a>
                      <a href=""><i className="fa fa-linkedin"></i></a>
                      <a href=""><i className="fa fa-google-plus"></i></a>
                      <a href=""><i className="fa fa-instagram"></i></a>
                      <a href=""><i className="fa fa-pinterest-p"></i></a>
                      <a href=""><i className="fa fa-dribbble"></i></a>
                  </div>
              </div>
              <div className="col-md-12 col-sm-12">
                  <div className="copyright_text">
                      <p className=" wow zoomIn" data-wow-duration="1s">Made with <i className="fa fa-heart"></i> by <a href="http://bootstrapthemes.co">Bootstrap Themes</a>2016. All Rights Reserved</p>
                  </div>
              </div>
          </div>
      </div>
  </footer>



  <div className="scrollup">
      <a href="#"><i className="fa fa-chevron-up"></i></a>
  </div>
</div>

)

export default IndexPage
