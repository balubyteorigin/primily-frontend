/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

// You can delete this file if you're not using it
import "./src/assets/css/animate/animate.css";
import "./src/assets/css/bootstrap/bootstrap.min.css";
import "./src/assets/css/fancybox/jquery.fancybox.css";
import "./src/assets/css/nivo-lightbox/nivo-lightbox.css";
import "./src/assets/css/themes/default/default.css";
import "./src/assets/css/animate/animate.css";
import "./src/assets/css/style.css";
import "./src/assets/css/responsive.css";